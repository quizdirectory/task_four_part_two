package framework;

import org.apache.logging.log4j.*;
import java.util.*;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

public class TUnit {

	private static List<String> checks;
	private static int checksMade = 0;
	private static int passedChecks = 0;
	private static int failedChecks = 0;

	private static Logger myLogger = LogManager.getLogger(TUnit.class);

	private static void addToReport(String txt) {
		if (checks == null) {
			checks = new LinkedList<String>();
		}
		checks.add(String.format("%04d: %s", checksMade++, txt));
	}

	public static void checkInstanceVariable(Object obj, String attributeName, double expectedValue) {
		try {

			Field field = obj.getClass().getDeclaredField(attributeName);
			field.setAccessible(true);

			double actualValue = field.getDouble(obj);

			if (Double.compare(actualValue, expectedValue) == 0) {

				addToReport(
						String.format("  %s.%s == %f", obj.getClass().getSimpleName(), attributeName, expectedValue));
				myLogger.info(
						String.format("  %s.%s == %f", obj.getClass().getSimpleName(), attributeName, expectedValue));

				passedChecks++;

			} else {

				addToReport(String.format("* %s.%s == %f (Expected Value: %f)", obj.getClass().getSimpleName(),
						attributeName, actualValue, expectedValue));
				myLogger.info(String.format("* %s.%s == %f (Expected Value: %f)", obj.getClass().getSimpleName(),
						attributeName, actualValue, expectedValue));

				failedChecks++;

			}
		} catch (Exception e) {

			addToReport(String.format("* Accessing instance variable failed: %s.%s", obj.getClass().getSimpleName(),
					attributeName));
			myLogger.error(String.format("* Accessing instance variable failed: %s.%s", obj.getClass().getSimpleName(),
					attributeName));

			failedChecks++;
		}
	}

	public static void checkMethodInvoke(Object obj, String methodName, double expectedValue) {

		try {

			Method method = obj.getClass().getDeclaredMethod(methodName);
			method.setAccessible(true);
			double actualValue = (double) method.invoke(obj);

			if (Double.compare(actualValue, expectedValue) == 0) {

				addToReport(
						String.format("  %s.%s() == %f", obj.getClass().getSimpleName(), methodName, expectedValue));
				myLogger.info(
						String.format("  %s.%s() == %f", obj.getClass().getSimpleName(), methodName, expectedValue));

				passedChecks++;
			} else {

				addToReport(String.format("* %s.%s() == %f (Expected Value: %f)", obj.getClass().getSimpleName(),
						methodName, actualValue, expectedValue));
				myLogger.info(String.format("* %s.%s() == %f (Expected Value: %f)", obj.getClass().getSimpleName(),
						methodName, actualValue, expectedValue));

				failedChecks++;
			}
		} catch (Exception e) {

			addToReport(String.format("* Method invoke failed: %s.%s()", obj.getClass().getSimpleName(), methodName));
			myLogger.error(
					String.format("* Method invoke failed: %s.%s()", obj.getClass().getSimpleName(), methodName));

			failedChecks++;

		}

	}
 
	public static void checkEquals(double valueOne, double valueTwo) {

		try {

			if (Double.compare(valueOne, valueTwo) == 0) {

				addToReport(String.format("  %f == %f", valueOne, valueTwo));
				myLogger.info(String.format("  %f == %f", valueOne, valueTwo));

				passedChecks++;

			} else {

				addToReport(String.format("* %f == %f", valueOne, valueTwo));
				myLogger.info(String.format("* %f == %f", valueOne, valueTwo));

				failedChecks++;

			}

		} catch (Exception e) {

			addToReport("An error occurred while performing the checkEquals.");
			myLogger.error("An error occurred while performing the checkEquals.");

			failedChecks++;

		}

	}

	public static void checkNotEquals(double valueOne, double valueTwo) {

		try {

			if (Double.compare(valueOne, valueTwo) != 0) {

				addToReport(String.format("  %f != %f", valueOne, valueTwo));
				myLogger.info(String.format("  %f != %f", valueOne, valueTwo));

				passedChecks++;

			} else {

				addToReport(String.format("* %f != %f", valueOne, valueTwo));
				myLogger.info(String.format("  %f != %f", valueOne, valueTwo));

				failedChecks++;

			}

		} catch (Exception e) {

			addToReport("An error occurred while performing the checkNotEquals.");
			myLogger.error("An error occurred while performing the checkEquals.");

			failedChecks++;

		}

	}

	public static void report() {

		System.out.println();
		System.out.printf("REPORT\n");
		System.out.printf("%d checks passed\n", passedChecks);
		System.out.printf("%d checks failed\n", failedChecks);
		System.out.println();

		for (String check : checks) {
			System.out.println(check);
		}

	}

	public static void runTests(Class<?> testClass) {

		try {

			Object testCalObj = testClass.getDeclaredConstructor().newInstance();
			Method[] methods = testClass.getDeclaredMethods();

			for (Method method : methods) {
				String methodName = method.getName();
				if (methodName.startsWith("check")) {
					try {
						method.setAccessible(true);
						method.invoke(testCalObj);
					} catch (Exception e) {
						addToReport(String.format("* Test method execution failed: %s", methodName));
						myLogger.error("Error executing test method", e);

						failedChecks++;
					}
				}
			}

		} catch (Exception e) {
			myLogger.error("Error running tests", e);
		}

	}

}
