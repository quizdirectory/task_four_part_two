package task_four_part_two;

import static framework.TUnit.*;

public class TestCalculate {

	public void checkConstructorAndAccess() {

		Calculate calObj = new Calculate(3.5, 4.5);
		checkEquals(calObj.getValueOne(), 3.5);
		checkEquals(calObj.getValueTwo(), 4.5);

		checkNotEquals(calObj.getValueTwo(), 4.5);
		checkNotEquals(calObj.getValueTwo(), 4);

	}

	public void checkMultipliedValueOne() {

		Calculate calObj = new Calculate(3.5, 4.5);
		calObj.setMultiplier(3);
		calObj.multipliedValueOne();
		checkEquals(calObj.getValueOne(), 10.5);

	}

	public void checkVariable() {

		Calculate calObj = new Calculate(3.5, 4.5);
		checkInstanceVariable(calObj, "valueTwo", 4.5);

	}

	public void checkMethod() {

		Calculate calObj = new Calculate(3.5, 4.5);
		checkMethodInvoke(calObj, "getValueOne", 3.1);

	}

	public static void main(String[] args) {

		runTests(TestCalculate.class);
		report();

	}

}
